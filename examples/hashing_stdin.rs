use aen::hashing::Hasher;
use std::io;
fn main() {
    let mut output = [u8::default(); 256 / 8];
    let mut hasher = aen::hashing::Cash::CASH256();

    let mut linebuf = String::new();

    io::stdin()
        .read_line(&mut linebuf)
        .expect("Coulldnt read from stdin");
    // println!("Input: {}", linebuf);
    hasher.update(linebuf.as_bytes());

    hasher.finalize(&mut output);
    // println!("Hash output: {:02x?}", output);
    aen::print_slice_as_hex_raw(&output);
}

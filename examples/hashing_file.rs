use aen::hashing::Hasher;
use std::env;
use std::fs::File;
use std::io::{BufReader, Read};
fn main() {
    let mut output = [u8::default(); 256 / 8];
    let mut hasher = aen::hashing::Cash::CASH256();

    // let input = File::open("input.data.large").expect("input.data file not
    // found");
    let mut args = env::args();
    let _ = &args.next();
    let infilepath = &args.next().expect("No input file specified");
    let input = File::open(infilepath).expect("Unable to open input file");
    let mut reader = BufReader::new(input);
    let mut buffer = [0; 4096];

    loop {
        let count = reader.read(&mut buffer).unwrap();
        if count == 0 {
            break;
        }
        hasher.update(&buffer[..count]);
    }

    hasher.finalize(&mut output);
    // println!("Hash output: {:02x?}", output);
    aen::print_slice_as_hex(&output, "Hash Output");
}

use aen::hashing::Hasher;
fn main() {
    let mut output = [u8::default(); 256 / 8];
    let mut hasher = aen::hashing::Cash::CASH256();

    // hasher.update(
    // b"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
    // eiusmod tempor incididunt ut labore et dolore magna aliqua. Libero nunc
    // consequat interdum varius sit amet mattis vulputate enim. Ultrices dui
    // sapien eget mi proin sed libero. Lorem ipsum dolor sit amet consectetur
    // adipiscing elit pellentesque.",
    // );
    hasher.update(b"a");
    hasher.finalize(&mut output);
    aen::print_slice_as_hex(&output, "Hash Output");
}

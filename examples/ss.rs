struct Myhash {
    inp: Vec<u8>,
}

impl aen::hashing::Hasher for Myhash {
    fn update(&mut self, input: &[u8]) {
        // println!("Update Input: {:?}", input);
        self.inp.extend_from_slice(input);
    }

    fn finalize(&mut self, output: &mut [u8]) {
        // println!("Finalise Output: {:?}", self.inp);
        output[..std::cmp::min(self.inp.len(), 32)]
            .copy_from_slice(&self.inp[..std::cmp::min(self.inp.len(), 32)]);
    }
}
fn main() {
    let secrets = ["Hello World", "AEN Hashing Scheme"];
    let shares = ["6W3WAYiYUYx1yEZy4i1gWQ", "OCiPoFw9He4OX6typQOAdB"];
    let access =
        vec![vec![vec![0, 1], vec![0, 1]], vec![vec![0, 1], vec![0, 1]]];

    let instate = aen::secret_sharing::AENInState {
        secrets: Vec::from(secrets),
        shares:  Vec::from(shares),
        access:  access,
    };


    let hashfn = aen::hashing::Cash::CASH256();
    // println!("SS input: {:#?}", instate);
    let output = aen::secret_sharing::share(instate, hashfn);
    println!("SS Output: {:02x?}", output);
}

//! Secret Sharing Part of the AEN scheme


/// Sharable inputs
pub trait Shareable {
    /// Conversion
    fn to_slice(&self) -> &[u8];
}


impl Shareable for &str {
    fn to_slice(&self) -> &[u8] {
        self.as_bytes()
    }
}
impl Shareable for &[u8] {
    fn to_slice(&self) -> &[u8] {
        self
    }
}


// type Secret = Shareable;
type Share<'a> = &'a [u8];
/// TODO: parameterise this for different hash lengths
// type HashDig = Vec<&str>;

/// AEN Secret Sharing Scheme
pub fn share<T: crate::hashing::Hasher, U: Shareable>(
    input: AENInState<U>,
    mut hashfn: T,
) -> AENOutState<[u8; 32]> {
    // Verify that access structures are provided for all secrets
    assert!(input.secrets.len() == input.access.len());


    // Buffers to use for hashed output
    let mut hashing_buffer = [0u8; 32];
    let mut h2buffer = [0u8; 32];

    // Step 1: For i=1,2,...,k; j=1,2,...,t_i, the dealer computes S_ij
    // For each secret..
    let mut Hs = Vec::with_capacity(input.secrets.len());
    let (Sj, H2aj): (Vec<Vec<[u8; 32]>>, Vec<Vec<Vec<[u8; 32]>>>) = input
        .secrets
        .iter()
        .enumerate()
        .map(|(secret_idx, secret)| {
            // For each access structure of a secret
            // let S__H2a: Vec<([u8; 32], Vec<[u8; 32]>)> = input.access
            let (S, H2a): (Vec<[u8; 32]>, Vec<Vec<[u8; 32]>>) = input.access
                [secret_idx]
                .iter()
                .enumerate()
                .map(|(aset_idx, pset)| {
                    let mut H2a_ij = Vec::new();
                    // For each participant involved in an access structure,
                    // Combine their shares
                    let mut hashed_xor =
                        pset.iter().fold([0; 32], |mut hashed_xor, p_idx| {
                            // Computing H(a|i|j)
                            hashfn.update(
                                input.secrets[p_idx.to_owned()].to_slice(),
                            );
                            hashfn.update(&[secret_idx as u8, aset_idx as u8]);
                            hashfn.finalize(hashing_buffer.as_mut());

                            // Computing H^2(a|i|j)
                            hashfn.update(&hashing_buffer);
                            hashfn.finalize(h2buffer.as_mut());
                            H2a_ij.push(h2buffer);

                            // H(a|i|j) XORed for each participant in access
                            // structure
                            crate::xor_slices(
                                hashed_xor.as_mut(),
                                hashing_buffer.as_ref(),
                            );
                            hashed_xor
                        });
                    // XOR the combined shares with the secret
                    // to produce S (for ith secret and jth auth set)
                    crate::xor_slices(
                        hashed_xor.as_mut(), // this is now S
                        secret.to_slice(),
                    );
                    // Return and collect S and H^2(s)
                    (hashed_xor, H2a_ij)
                })
                .unzip();

            hashfn.update(secret.to_slice());
            hashfn.finalize(hashing_buffer.as_mut()); // hashing_buffer is now H(s)

            // Collect H(s)
            Hs.push(hashing_buffer);

            // Return and collect S and H^2(s)
            (S, H2a)
        })
        .unzip();

    AENOutState {
        hashed_secrets:  Hs,
        hashsqed_shares: H2aj,
        publicshares:    Sj,
    }
}

/// Input state for the secret sharing scheme
#[derive(Debug)]
pub struct AENInState<T: Shareable> {
    /// The list of secrets to be shared
    pub secrets: Vec<T>,
    /// The list of private shares to be used
    /// here, `share[0]` correspond to the secret share of the `0th`
    /// participant
    pub shares:  Vec<T>,
    /// The access structre for the scheme such that
    /// `access[0]` is the list of access structures for `secret[0]`
    /// and elements in `access[0]` are the indexes of the share
    /// corresponding to each participant from `shares`
    pub access:  Vec<Vec<Vec<usize>>>,
}
/// Output from secret sharing scheme
#[derive(Debug, Default)]
pub struct AENOutState<T> {
    /// The public shares `S_i_j` for each access structure
    pub publicshares: Vec<Vec<T>>,

    /// Hashed shares `H(\alpha, i, j)`
    pub hashsqed_shares: Vec<Vec<Vec<T>>>,

    /// Hashed secrets `H(s_i)`
    pub hashed_secrets: Vec<T>,
}

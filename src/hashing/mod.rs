//! Internal Hashing module for the AEN scheme

use crate::{CAPACITY, RATE};
use std::io::{self, Write};

mod cash;
pub use cash::Cash;

/// Trait that each hashing algorithm needs to implement
/// Add data to be hashed
pub trait Hasher {
    /// Add data to be hashed
    fn update(&mut self, input: &[u8]);

    /// Finalise the data and calculate the hash
    fn finalize(&mut self, output: &mut [u8]);
}

// #[derive(Default, Clone)]
/// Buffer for the sponge construction
// pub struct Buffer([u8; WORDS]);
type Buffer = [u8; (RATE + CAPACITY) / 8];

/// Internal sponge function trait
/// A type has to implement this inorder to be used as the internal function for
/// the sponge construction
pub trait SpongeFn {
    /// The internal transformation function that is called by the sponge
    fn internal(&mut self, buffer: &mut Buffer);
    /// Reset the internal state if any
    fn reset(&mut self);
}

#[derive(Debug)]
enum SpongeState {
    Absorbing,
    Squeezing,
}
/// Sponge construction as defined by the [keccack team](https://keccak.team/sponge_duplex.html)
///
/// Variants of the sponge construction are presented by this library, each
/// using a different internal function.
pub struct Sponge<T: SpongeFn> {
    /// Rate r of the sponge construction.
    rate: usize,

    /// Capacity c of the sponge construciton. c >= 2n for optimal security
    /// bounds
    capacity: usize,

    /// Hashlen n of the spong construction. This decides the amount of bts
    /// that needs to be squeezed from the sponge to form the final digest
    hashlen: usize,

    /// The buffer of length r+c used in the sponge construction. This buffer
    /// is passed to each iteration of the internal function while adding r
    /// bits from the input message to the first r bits of the buffer
    buffer: Buffer,

    /// A carryover buffer used to store leftover data when input data is not a
    /// multiple of r. This data is used up when another [`&self.update`]
    /// is called. If no update is called, it is padded in the 1 0* scheme
    /// to make a block of r bits to absorb
    carryover: Vec<u8>,

    /// The internal hash function of the sponge. This is a generic type that
    /// implements the [`SpongeFn`] trait.
    f: T,
}

// fn share_secrets(state: AENInState) -> AENOutState {}
impl<T: SpongeFn> Hasher for Sponge<T> {
    fn update(&mut self, input: &[u8]) {
        // println!("Got inp: {:02x?}", input);
        // println!("Input: {:02x?} | {}", &input, &input.len());
        /// incrementing pointer to keep track of the sponge progress
        let mut ip = 0;
        let mut l = input.len() * 8;
        let rate = self.rate;
        let co = self.carryover.len();

        /// Previously carried over bits remain to be processes.
        /// This can occour when update is called multiple times without
        /// finalizing In this case, start by processing the carried
        /// over bits + some bits from current input
        if co > 0 {
            let remaining = rate / 8 - co;
            /// Given input still not enough to fill r bits
            /// then, push to carryover and return
            if l / 8 < remaining {
                self.carryover.extend_from_slice(&input[..]);
                // println!("CarryOver: {:02x?}", &self.carryover);
                return;
            }
            self.carryover.extend_from_slice(&input[..remaining]);
            crate::xor_slices(self.buffer.as_mut(), &self.carryover);
            self.f.internal(&mut self.buffer);

            ip += remaining * 8;
            l -= remaining * 8;
        }

        while l >= rate {
            // TODO: this xor is byte-padded :(
            // think when r = 12
            // make a proper bit level xor

            /// XOR the buffer oof size r with input of size r, starting with
            /// the byte pointed by `ip`
            /// then call `f()` on the buffer
            crate::xor_slices(
                self.buffer.as_mut(),
                &input[ip / 8..(ip + rate) / 8],
            );
            self.f.internal(&mut self.buffer);
            // println!("BUFFER: {:02x?}", &self.buffer);

            ip += rate;
            l -= rate;
        }

        // Save the carryover bytes when less than `rate` bits remain to be
        // processed
        self.carryover = input[ip / 8..].into();
        // println!("CarryOver: {:02x?}", &self.carryover);
    }

    fn finalize(&mut self, output: &mut [u8]) {
        let rate = self.rate;
        // Padding: 1 0* 1
        // Push 10000_0000 byte first
        // Then push required no of 0000_0000 bytes
        // Finish with a 0000_0001 byte
        let carry_length = self.carryover.len();
        let padlen = rate / 8 - carry_length;
        self.carryover.push(0x80);
        for _ in 0..padlen - 2 {
            self.carryover.push(0x0);
        }
        self.carryover.push(0x01);
        // println!("CarryOverPadded: {:02x?}", self.carryover);

        crate::xor_slices(self.buffer.as_mut(), &self.carryover);
        self.f.internal(&mut self.buffer);


        // // Static squeezing for rate=88, h=128
        // debug_assert_eq!(rate, 88);
        // debug_assert_eq!(self.hashlen, 128);
        // output[0..11].copy_from_slice(&self.buffer[..11]);
        // (self.f)(&mut self.buffer);
        // output[11..].copy_from_slice(&self.buffer[..5]);

        // let stdout = io::stdout();
        // let mut handle = stdout.lock();
        // loop {
        //     // &self.buffer[..rate / 8]
        //     // .iter()
        //     // .for_each(|x| print!("{:02x}", x));
        //     handle
        //         .write(&self.buffer[..rate / 8])
        //         .expect("Couldnt write to stdout");
        //     // output[startidx..endidx]
        //     .copy_from_slice(&self.buffer[..lenbytes]);

        //     self.f.internal(&mut self.buffer);
        // }

        // Squeezing starts
        let mut ip: usize = 0;
        let mut l = self.hashlen;
        while l >= rate {
            let startidx = ip / 8;
            let endidx = (ip + rate) / 8;
            let lenbytes = endidx - startidx;

            // println!(
            //     "COPY {:02x?} \t {}..{} | 0..{}",
            //     &self.buffer[..lenbytes],
            //     startidx,
            //     endidx,
            //     lenbytes
            // );
            output[startidx..endidx].copy_from_slice(&self.buffer[..lenbytes]);

            self.f.internal(&mut self.buffer);
            ip += rate;
            l -= rate;
        }
        // println!(
        //     "COPY {:02x?} \t {}.. | 0..{}",
        //     &self.buffer[..l / 8],
        //     ip / 8,
        //     l / 8
        // );
        output[ip / 8..].copy_from_slice(&self.buffer[..l / 8]);

        // Cleanup
        self.carryover.clear();
        self.buffer.fill(0);
        self.f.reset();
    }
}

// impl<T: SpongeFn> Sponge<T> {
//     /// Create a new instance of a sponge with the parameters:
//     /// rate: r
//     /// capacity: c
//     /// hashlen: n
//     /// f: internal function
//     pub fn new<T>(rate: usize, capacity: usize, hashlen: usize, internal: T)
// -> Self {         Sponge {
//             buffer: Buffer::default(),
//             offset: 0,
//             rate,
//             capacity,
//             hashlen,
//             internal,
//             state: SpongeState::Absorbing,
//             carryover: Vec::with_capacity(rate),
//         }
//     }
// }

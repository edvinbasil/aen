//! CASH [@kuila2014] implementation
//!
//! [@kuila2014]: https://link.springer.com/chapter/10.1007/978-3-319-12060-7_5
//!
//! # Examples
//!
//! Use cash as the internal function for a sponge construction as follows
//!
//! ```
//! use aen::hashing::Hasher;
//!
//! let mut hasher = aen::hashing::Cash::CASH256();
//!
//! let mut output = [u8::default(); 256 / 8];
//!
//! hasher.update(b"Hello World");
//! hasher.finalize(&mut output);
//! ```

extern crate test;

use crate::{
    hashing::Buffer, print_slice_as_bin, print_slice_as_hex, CAPACITY, RATE,
};

use super::Sponge;
use super::SpongeFn;

const KAPPA: u8 = 0b00000110;

/// CASH structure
#[derive(Debug)]
pub struct Cash {
    kappaState: u8,
}

impl SpongeFn for Cash {
    /// CASH function to be used as the internal function for a sponge
    /// construction
    fn internal(&mut self, input: &mut Buffer) {
        // // print_slice_as_hex(input, "cash inp");

        // layer_permute(input);
        // // print_slice_as_hex(input, "perm out");

        // layer_nonlinear(input);
        // // print_slice_as_hex(input, "nonl out");

        // self.layer_linear(input, RATE);
        // // print_slice_as_hex(input, "linr out");

        // // print!("--------------------\n");

        // use crunchy::unroll;
        // unroll! {
        //     for a in 0..15 {
        //         layer_permute_unrolled(input);
        //         layer_nonlinear(input);
        //         self.layer_linear::<5>(input);
        //     }
        // }

        // use crunchy::unroll;
        // unroll! {
        //     for a in 0..10 {
        //         self.layer_linear::<3>(input);
        //         layer_permute_unrolled(input);
        //         layer_nonlinear(input);
        //         layer_permute_unrolled(input);
        //         self.layer_linear::<3>(input);
        //     }
        // }

        self.layer_linear::<{ RATE / 2 }>(input);
        layer_permute_unrolled(input);
        layer_nonlinear(input);
        layer_permute_unrolled(input);
        self.layer_linear::<{ RATE / 4 }>(input);
    }

    fn reset(&mut self) {
        self.kappaState = KAPPA;
    }
}

impl Cash {
    /// Create the CASH256 variant
    /// This variant of the hash function has a digest length of 256
    /// The values of the internal parameters are
    ///
    /// r: 248
    /// c: 264
    pub fn CASH256() -> Sponge<Cash> {
        const R: usize = 248;
        const C: usize = 264;
        const N: usize = 256;

        Self::new(R, C, N)
    }

    /// Constructs a new Sponge with cash as the internal function
    pub fn new(r: usize, c: usize, n: usize) -> Sponge<Cash> {
        // crate::hashing::Sponge::new(r, c, n, cash);
        Sponge {
            buffer:    [0u8; (RATE + CAPACITY) / 8],
            rate:      r,
            capacity:  c,
            hashlen:   n,
            f:         Cash { kappaState: KAPPA },
            carryover: Vec::with_capacity(r),
        }
    }
    fn layer_linear<const CYCLES: usize>(&mut self, buf: &mut Buffer) {
        let mut ruleNumber = [0u8; (RATE + CAPACITY) / 8];
        // println!("KappaState: {}", self.kappaState);
        expandKappa(self.kappaState, &mut ruleNumber);
        // print_slice_as_hex(&ruleNumber, "ruleA");
        unsafe { rule_90_150::<CYCLES>(buf, &mut ruleNumber) };
        self.updateRule();
    }
    fn updateRule(&mut self) {
        let a: u8 = self.kappaState << 1;
        let b: u8 = self.kappaState >> 1;

        // println!("KappaStateB: {:x}", self.kappaState);
        self.kappaState = a ^ (self.kappaState & KAPPA) ^ b;
        // println!("KappaStateA: {:x}", self.kappaState);
    }
}

unsafe fn rule_90_150<const CYCLES: usize>(
    buf: &mut Buffer,
    ruleNumber: &mut Buffer,
) {
    // consists of n_r rounds
    let mut leftShiftedState = [0u8; (RATE + CAPACITY) / 8];
    let mut rightShiftedState = [0u8; (RATE + CAPACITY) / 8];

    for i in 0..(RATE + CAPACITY) / 64 {
        *(buf.as_mut_ptr().add(i * 8) as *mut u64) =
            (*(buf.as_ptr().add(i * 8) as *const u64)).swap_bytes();
        *(ruleNumber.as_mut_ptr().add(i * 8) as *mut u64) =
            (*(ruleNumber.as_ptr().add(i * 8) as *const u64)).swap_bytes();
    }

    for _i in 0..CYCLES {
        // shr_64_sized_swapbytes::<{RATE+CAPACITY}>(buf, &mut
        // rightShiftedState); shl_64_sized_swapbytes::
        // <{RATE+CAPACITY}>(buf, &mut leftShiftedState);
        shifts_64_sized_swapbytes::<{ RATE + CAPACITY }>(
            buf,
            &mut leftShiftedState,
            &mut rightShiftedState,
        );
        xor_and_apply_rule::<{ RATE + CAPACITY }>(
            buf,
            &leftShiftedState,
            &rightShiftedState,
            ruleNumber,
        );
    }
    for i in 0..(RATE + CAPACITY) / 64 {
        *(buf.as_mut_ptr().add(i * 8) as *mut u64) =
            (*(buf.as_ptr().add(i * 8) as *const u64)).swap_bytes();
    }
}
unsafe fn xor_and_apply_rule<const N: usize>(
    buf: &mut [u8],
    ls: &[u8],
    rs: &[u8],
    rule: &[u8],
) {
    // for (((l, r), a_rule), a_buf) in
    // ls.iter().zip(rs).zip(rule).zip(buf.iter_mut()) {     *a_buf = *a_buf
    // & *a_rule ^ *l ^ *r; }
    for i in 0..(N / 64) {
        *(buf.as_mut_ptr().add(i * 8) as *mut u64) = (*(buf.as_ptr().add(i * 8)
            as *const u64)
            & *(rule.as_ptr().add(i * 8) as *const u64))
            ^ *(rs.as_ptr().add(i * 8) as *const u64)
            ^ *(ls.as_ptr().add(i * 8) as *const u64);
    }
    if (N % 64) >= 32 {
        *(buf.as_mut_ptr().add((N / 64) * 8) as *mut u32) =
            (*(buf.as_ptr().add((N / 64) * 8) as *const u32)
                & *(rule.as_ptr().add((N / 64) * 8) as *const u32))
                ^ *(rs.as_ptr().add((N / 64) * 8) as *const u32)
                ^ *(ls.as_ptr().add((N / 64) * 8) as *const u32);
    }
    if (N % 32) >= 16 {
        *(buf.as_mut_ptr().add((N / 32) * 4) as *mut u16) =
            (*(buf.as_ptr().add((N / 32) * 4) as *const u16)
                & *(rule.as_ptr().add((N / 32) * 4) as *const u16))
                ^ *(rs.as_ptr().add((N / 32) * 4) as *const u16)
                ^ *(ls.as_ptr().add((N / 32) * 4) as *const u16);
    }
    if (N % 16) >= 8 {
        *(buf.as_mut_ptr().add((N / 16) * 2) as *mut u8) =
            (*(buf.as_ptr().add((N / 16) * 2) as *const u8)
                & *(rule.as_ptr().add((N / 16) * 2) as *const u8))
                ^ *(rs.as_ptr().add((N / 16) * 2) as *const u8)
                ^ *(ls.as_ptr().add((N / 16) * 2) as *const u8);
    }
}

unsafe fn shifts_64_sized_swapbytes<const N: usize>(
    a: &[u8],
    ls: &mut [u8],
    rs: &mut [u8],
) {
    for i in 0..(N / 64) {
        *(rs.as_mut_ptr().add(i * 8) as *mut u64) =
            (*(a.as_ptr().add(i * 8) as *const u64)) >> 1;
        *(ls.as_mut_ptr().add(i * 8) as *mut u64) =
            (*(a.as_ptr().add(i * 8) as *const u64)) << 1;
    }

    // Fixup carry bits
    *(rs.as_mut_ptr().add(7) as *mut u8) &= 0b01111111;
    for i in 1..(N / 64) {
        *(rs.as_mut_ptr().add((i + 1) * 8 - 1) as *mut u8) &= 0b01111111;
        *(rs.as_mut_ptr().add((i + 1) * 8 - 1) as *mut u8) |=
            (*(a.as_ptr().add((i - 1) * 8) as *const u8)) << 7;
        *(ls.as_mut_ptr().add((i - 1) * 8) as *mut u8) |=
            (*(a.as_ptr().add((i + 1) * 8 - 1) as *const u8) >> 7) & 1;
    }
}
unsafe fn shr_64_sized_swapbytes<const N: usize>(a: &[u8], b: &mut [u8]) {
    for i in 0..(N / 64) {
        *(b.as_mut_ptr().add(i * 8) as *mut u64) =
            ((*(a.as_ptr().add(i * 8) as *mut u64)).swap_bytes() >> 1)
                .swap_bytes();
    }
    if (N % 64) >= 32 {
        *(b.as_mut_ptr().add((N / 64) * 8) as *mut u32) =
            ((*(a.as_ptr().add((N / 64) * 8) as *mut u32)).swap_bytes() >> 1)
                .swap_bytes();
    }
    if (N % 32) >= 16 {
        *(b.as_mut_ptr().add((N / 32) * 4) as *mut u16) =
            ((*(a.as_ptr().add((N / 32) * 4) as *mut u16)).swap_bytes() >> 1)
                .swap_bytes();
    }
    if (N % 16) >= 8 {
        *(b.as_mut_ptr().add((N / 16) * 2) as *mut u8) =
            ((*(a.as_ptr().add((N / 16) * 2) as *mut u8)).swap_bytes() >> 1)
                .swap_bytes();
    }
    // Fixup carry bits
    for i in 0..(N / 64) {
        *(b.as_mut_ptr().add((i + 1) * 8) as *mut u8) |=
            (*(a.as_ptr().add((i + 1) * 8 - 1) as *mut u8)) << 7;
    }
    if (N % 64) > 32 {
        *(b.as_mut_ptr().add((N / 32) * 4) as *mut u8) |=
            (*(a.as_ptr().add((N / 32) * 4 - 1) as *mut u8)) << 7;
    }
    if (N % 32) > 16 {
        *(b.as_mut_ptr().add((N / 16) * 2) as *mut u8) |=
            (*(a.as_ptr().add((N / 16) * 2 - 1) as *mut u8)) << 7;
    }
}

unsafe fn shl_64_sized_swapbytes<const N: usize>(a: &[u8], b: &mut [u8]) {
    for i in 0..(N / 64) {
        *(b.as_mut_ptr().add(i * 8) as *mut u64) =
            ((*(a.as_ptr().add(i * 8) as *mut u64)).swap_bytes() << 1)
                .swap_bytes();
    }
    if (N % 64) >= 32 {
        *(b.as_mut_ptr().add((N / 64) * 8) as *mut u32) =
            ((*(a.as_ptr().add((N / 64) * 8) as *mut u32)).swap_bytes() << 1)
                .swap_bytes();
    }
    if (N % 32) >= 16 {
        *(b.as_mut_ptr().add((N / 32) * 4) as *mut u16) =
            ((*(a.as_ptr().add((N / 32) * 4) as *mut u16)).swap_bytes() << 1)
                .swap_bytes();
    }
    if (N % 16) >= 8 {
        *(b.as_mut_ptr().add((N / 16) * 2) as *mut u8) =
            ((*(a.as_ptr().add((N / 16) * 2) as *mut u8)).swap_bytes() << 1)
                .swap_bytes();
    }
    // Fixup carry bits
    for i in 1..(N / 64) {
        *(b.as_mut_ptr().add(i * 8 - 1) as *mut u8) |=
            (*(a.as_ptr().add(i * 8) as *mut u8) >> 7) & 1;
    }
    if (N % 64) >= 32 {
        *(b.as_mut_ptr().add((N / 64) * 8 - 1) as *mut u8) |=
            (*(a.as_ptr().add((N / 64) * 8) as *mut u8) >> 7) & 1;
    }
    if (N % 32) >= 16 {
        *(b.as_mut_ptr().add((N / 32) * 4 - 1) as *mut u8) |=
            (*(a.as_ptr().add((N / 32) * 4) as *mut u8) >> 7) & 1;
    }
    if (N % 16) >= 8 {
        *(b.as_mut_ptr().add((N / 16) * 2 - 1) as *mut u8) |=
            (*(a.as_ptr().add((N / 16) * 2) as *mut u8) >> 7) & 1;
    }
}

fn expandKappa(k: u8, ruleNumber: &mut Buffer) {
    let kbar = k ^ 0b10000001;
    let kbar_rev = kbar.reverse_bits();

    static_assertions::const_assert!((RATE + CAPACITY) / 16 == 32);
    use crunchy::unroll;
    unroll! {
        for i in 0..32 {
            ruleNumber[2 * i] = kbar;
            ruleNumber[2 * i + 1] = kbar_rev;
        }
    }

    ruleNumber[0] ^= 0b10000000;
    ruleNumber[(RATE + CAPACITY) / 8 - 1] ^= 1;
}

fn swap_bits_in_buffer(buf: &mut Buffer, pos1: usize, pos2: usize) {
    let byte1 = buf[pos1 / 8];
    let byte2 = buf[pos2 / 8];

    let bit1 = byte1 >> (7 - pos1 % 8) & 1;
    let bit2 = byte2 >> (7 - pos2 % 8) & 1;

    buf[pos1 / 8] ^= (bit1 ^ bit2) << (7 - pos1 % 8);
    buf[pos2 / 8] ^= (bit1 ^ bit2) << (7 - pos2 % 8);
}

fn layer_permute(buf: &mut Buffer) {
    for i in 0..RATE {
        // let pos1 = RATE + i;
        // let pos2 = ((i + 1) * 11) % (CAPACITY - 1);

        ///           +---r bits---+   +------c bits------+
        ///           |            |   |                  |
        /// Buffer: [(0).........(r-1)(r)...............(r+c-1)]
        ///
        /// Swap bits in the r part (bits [0..r-1])
        /// with bits in the c part (bits [r..c+r-1])
        /// at 11 bit intervals
        let pos1 = i;
        let pos2 = RATE + (i * 11) % (CAPACITY - 1);
        // println!("SWAP {} - {}", pos1, pos2);
        swap_bits_in_buffer(buf, pos1, pos2);
    }
}
fn layer_permute_unrolled(buf: &mut Buffer) {
    use crunchy::unroll;
    static_assertions::const_assert!(RATE == 248);
    unroll! {
        for i in 0..248 {
            let pos2 = RATE + (i * 11) % (CAPACITY - 1);
            swap_bits_in_buffer(buf, i, pos2);
        }
    }
}
fn layer_permute_unrolled_diffusion<const DIST: usize>(buf: &mut Buffer) {
    use crunchy::unroll;
    static_assertions::const_assert!(RATE == 248);
    unroll! {
        for i in 0..248 {
            let pos2 = RATE + (i * DIST) % (CAPACITY - 1);
            swap_bits_in_buffer(buf, i, pos2);
        }
    }
}
fn layer_permute_unrolled_13(buf: &mut Buffer) {
    use crunchy::unroll;
    static_assertions::const_assert!(RATE == 248);
    unroll! {
        for i in 0..248 {
            let pos2 = RATE + (i * 13) % (CAPACITY - 1);
            swap_bits_in_buffer(buf, i, pos2);
        }
    }
}
fn layer_permute_unrolled_17(buf: &mut Buffer) {
    use crunchy::unroll;
    static_assertions::const_assert!(RATE == 248);
    unroll! {
        for i in 0..248 {
            let pos2 = RATE + (i * 17) % (CAPACITY - 1);
            swap_bits_in_buffer(buf, i, pos2);
        }
    }
}
fn layer_nonlinear(buf: &mut Buffer) {
    const SBOX: [u8; 16] = [
        0x0E, 0x09, 0x0F, 0x00, 0x0D, 0x04, 0x0A, 0x0B, 0x01, 0x02, 0x08, 0x03,
        0x07, 0x06, 0x0C, 0x05,
    ];
    for byte in buf {
        let leftpart = (*byte & 0xF0) >> 4;
        let rightpart = *byte & 0x0F;
        let leftsub = SBOX[leftpart as usize];
        let rightsub = SBOX[rightpart as usize];
        *byte = leftsub << 4 | rightsub;
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn non_linear() {
        let mut inbuf: [u8; 28] = [
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a,
            0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0xf0, 0xe1, 0xd2, 0xc3, 0xb4, 0xa5,
            0x96, 0x87, 0x78, 0x69, 0x5a, 0xff,
        ];
        let outbuf: [u8; 28] = [
            0xee, 0xe9, 0xef, 0xe0, 0xed, 0xe4, 0xea, 0xeb, 0xe1, 0xe2, 0xe8,
            0xe3, 0xe7, 0xe6, 0xeb, 0xe5, 0x5e, 0xb9, 0x6f, 0x70, 0x3d, 0x84,
            0x2a, 0x1b, 0xb1, 0xa2, 0x48, 0x55,
        ];

        // layer_nonlinear(&mut inbuf);
        // assert_eq!(inbuf, outbuf);
    }
    #[test]
    fn permutation() {
        let mut buffer: [u8; 28] = [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        ];
        let mut buffer_check = buffer.clone();
        // layer_permute_unrolled(&mut buffer);
        // layer_permute(&mut buffer_check);
        assert_eq!(buffer, buffer_check);
    }
    #[test]
    fn expansion() {
        let mut ruleNumber = [0u8; (RATE + CAPACITY) / 8];
        print_slice_as_bin(&ruleNumber, "RuleBefore");
        expandKappa(0b00111001, &mut ruleNumber);
        print_slice_as_bin(&ruleNumber, "RuleAfter ");
    }

    #[test]
    fn shl() {
        let input: [u8; 28] = [
            0x72, 0x1e, 0x98, 0xba, 0xb5, 0xd8, 0xdd, 0x04, 0x73, 0x3b, 0xf2,
            0x45, 0x84, 0x73, 0x9d, 0xb9, 0x3e, 0xa6, 0xde, 0x6e, 0x17, 0x2e,
            0x3b, 0x17, 0xa3, 0x75, 0x5e, 0x6d,
        ];
        let true_out: [u8; 28] = [
            0xe4, 0x3d, 0x31, 0x75, 0x6b, 0xb1, 0xba, 0x08, 0xe6, 0x77, 0xe4,
            0x8b, 0x08, 0xe7, 0x3b, 0x72, 0x7d, 0x4d, 0xbc, 0xdc, 0x2e, 0x5c,
            0x76, 0x2f, 0x46, 0xea, 0xbc, 0xda,
        ];
        let mut out = [0u8; 28];
        unsafe { shl_64_sized_swapbytes::<224>(&input, &mut out) };
        assert_eq!(out, true_out);
    }
    #[test]
    fn shr() {
        let input: [u8; 28] = [
            0x72, 0x1e, 0x98, 0xba, 0xb5, 0xd8, 0xdd, 0x04, 0x73, 0x3b, 0xf2,
            0x45, 0x84, 0x73, 0x9d, 0xb9, 0x3e, 0xa6, 0xde, 0x6e, 0x17, 0x2e,
            0x3b, 0x17, 0xa3, 0x75, 0x5e, 0x6d,
        ];
        let true_out: [u8; 28] = [
            0x39, 0x0f, 0x4c, 0x5d, 0x5a, 0xec, 0x6e, 0x82, 0x39, 0x9d, 0xf9,
            0x22, 0xc2, 0x39, 0xce, 0xdc, 0x9f, 0x53, 0x6f, 0x37, 0x0b, 0x97,
            0x1d, 0x8b, 0xd1, 0xba, 0xaf, 0x36,
        ];
        let mut out = [0u8; 28];
        unsafe { shr_64_sized_swapbytes::<224>(&input, &mut out) };
        assert_eq!(out, true_out);
    }
}
#[cfg(test)]
mod benches {
    use super::*;
    use test::{black_box, Bencher};

    #[bench]
    fn bench_kappa_expansion_cport(b: &mut Bencher) {
        let mut ruleNumber = [0u8; (RATE + CAPACITY) / 8];
        let kappa = 0b01011001;

        b.iter(|| {
            // Inner closure, the actual test
            for _ in 0..100 {
                expandKappa(black_box(kappa), &mut ruleNumber);
                black_box(ruleNumber);
            }
        });
    }
    #[bench]
    fn bench_shl(b: &mut Bencher) {
        let inbuf: [u8; 28] = [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        ];
        let mut out = [0u8; (RATE + CAPACITY) / 8];

        b.iter(|| {
            // Inner closure, the actual test
            for _ in 0..100 {
                unsafe {
                    shl_64_sized_swapbytes::<224>(black_box(&inbuf), &mut out)
                };
                black_box(out);
            }
        });
    }
    #[bench]
    fn bench_shr(b: &mut Bencher) {
        let inbuf: [u8; 28] = [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        ];
        let mut out = [0u8; (RATE + CAPACITY) / 8];

        b.iter(|| {
            // Inner closure, the actual test
            for _ in 0..100 {
                unsafe {
                    shr_64_sized_swapbytes::<224>(black_box(&inbuf), &mut out)
                };
                black_box(out);
            }
        });
    }
}

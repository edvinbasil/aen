//! # AEN
//! Secret Sharing using Cellular Automata
//! Reference Implementation

#![feature(test)]
#![deny(missing_docs)]
// #![warn(missing_doc_code_examples)]
#![allow(dead_code)]
#![allow(unused_doc_comments)]
#![allow(non_snake_case)]
#![deny(clippy::missing_docs_in_private_items)]
// #![deny(clippy::all)]
// #![deny(clippy::all, clippy::pedantic)]

const RATE: usize = 248;
const CAPACITY: usize = 264;

pub mod hashing;
pub mod secret_sharing;

use std::io::{self, Write};

fn xor_slices(a: &mut [u8], b: &[u8]) {
    // print!("XORIN {:02x?}\n", b);
    unsafe { xor_64_sized_unsafe_cast::<RATE>(a, b) };
    // xor_slices_unsafe_cast(a, b); // 1.679;
    // xor_slices_iter_bounds(a, b); // 1.705
    // xor_slices_iter(a, b); // 1.743
}
fn xor_slices_assert_bounds(a: &mut [u8], b: &[u8]) {
    assert_eq!(b.len(), RATE / 8);
    a.iter_mut().zip(b).for_each(|(x, y)| *x ^= *y)
}
fn xor_slices_iter(a: &mut [u8], b: &[u8]) {
    a.iter_mut().zip(b).for_each(|(x, y)| *x ^= *y)
}

fn xor_slices_iter_bounds(a: &mut [u8], b: &[u8]) {
    debug_assert_eq!(b.len(), RATE / 8);
    a.iter_mut()
        .zip(b[0..RATE / 8].iter())
        .for_each(|(x, y)| *x ^= *y)
}

fn xor_slices_unsafe_cast(a: &mut [u8], b: &[u8]) {
    debug_assert_eq!(b.len(), 11);
    unsafe {
        *(a.as_mut_ptr() as *mut u64) ^= *(b.as_ptr() as *const u64);
        *(a.as_mut_ptr().add(8) as *mut u16) ^=
            *(b.as_ptr().add(8) as *const u16);
        *(a.as_mut_ptr().add(10) as *mut u8) ^=
            *(b.as_ptr().add(10) as *const u8);
    }
}
unsafe fn xor_64_sized_unsafe_cast<const N: usize>(a: &mut [u8], b: &[u8]) {
    for i in 0..(N / 64) {
        *(a.as_mut_ptr().add(i * 8) as *mut u64) ^=
            *(b.as_ptr().add(i * 8) as *const u64);
    }
    if (N % 64) >= 32 {
        *(a.as_mut_ptr().add((N / 64) * 8) as *mut u32) ^=
            *(b.as_ptr().add((N / 64) * 8) as *const u32);
    }
    if (N % 32) >= 16 {
        *(a.as_mut_ptr().add((N / 32) * 4) as *mut u16) ^=
            *(b.as_ptr().add((N / 32) * 4) as *const u16);
    }
    if (N % 16) >= 8 {
        *(a.as_mut_ptr().add((N / 16) * 2) as *mut u8) ^=
            *(b.as_ptr().add((N / 16) * 2) as *const u8);
    }
}

/// Prints a slice of bytes as a space-seperated hex string
pub fn print_slice_as_hex(x: &[u8], msg: &str) {
    let stdout = io::stdout();
    let mut lock = stdout.lock();
    x.iter().for_each(|el| {
        write!(lock, "{:02X}", el).unwrap();
    });
    match write!(lock, " :{}\n", msg) {
        Ok(_) => (),
        Err(e) => eprintln!("Unable to write to stdout: {}", e),
    }
}
/// Prints a slice of bytes as a space-seperated hex string without description
pub fn print_slice_as_hex_raw(x: &[u8]) {
    let stdout = io::stdout();
    let mut lock = stdout.lock();
    x.iter().for_each(|el| {
        write!(lock, "{:02X}", el).unwrap();
    });
    match write!(lock, "\n") {
        Ok(_) => (),
        Err(e) => eprintln!("Unable to write to stdout: {}", e),
    }
}
/// Prints a slice of bytes as a space-seperated binary representation of 0s and
/// 1s
pub fn print_slice_as_bin(x: &[u8], msg: &str) {
    let stdout = io::stdout();
    let mut lock = stdout.lock();
    x.iter().for_each(|el| {
        write!(lock, "{:08b}", el).unwrap();
    });
    match write!(lock, " :{}\n", msg) {
        Ok(_) => (),
        Err(e) => eprintln!("Unable to write to stdout: {}", e),
    }
}
